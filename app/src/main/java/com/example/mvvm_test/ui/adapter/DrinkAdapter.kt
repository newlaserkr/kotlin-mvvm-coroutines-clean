package com.example.mvvm_test.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm_test.R
import com.example.mvvm_test.data.repository.model.Drink
import kotlinx.android.synthetic.main.recycler_item_drink.view.*

class DrinkAdapter(private val list: ArrayList<Drink>) :
    RecyclerView.Adapter<DrinkAdapter.DrinkViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder {
        return DrinkViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_drink, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: DrinkViewHolder, position: Int) {
        holder.tvName.text = list[position].name
    }

    class DrinkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView = itemView.text_view_name
    }
}

