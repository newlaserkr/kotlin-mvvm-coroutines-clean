package com.example.mvvm_test.data.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Drink(
    @PrimaryKey var name: String,
    var thumb: String
)