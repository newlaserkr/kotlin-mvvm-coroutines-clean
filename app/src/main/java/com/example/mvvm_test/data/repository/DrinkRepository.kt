package com.example.mvvm_test.data.repository

import androidx.lifecycle.LiveData
import com.example.mvvm_test.data.repository.model.Drink
import com.example.mvvm_test.data.storage.cloud.DrinkApiServiceImpl
import com.example.mvvm_test.data.storage.cloud.DrinksResponse
import com.example.mvvm_test.data.storage.database.room.DrinkDatabase
import kotlinx.coroutines.Deferred

class DrinkRepository {
      fun getRandomDrinkAsync(): Deferred<DrinksResponse>{
       return DrinkApiServiceImpl().getRandomDrinkAsync()
    }

    fun getSavedDrinksList(): List<Drink>? {
        return DrinkDatabase.getReadyDatabase()?.drinkDao?.getAll()
    }
}